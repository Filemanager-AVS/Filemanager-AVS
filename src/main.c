/*

Filename: main.c
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#include <gtk/gtk.h>
#include "application.h"

int main (int argc, char* argv[])
{
  return g_application_run (G_APPLICATION (favs_new ()), argc, argv);
}
