/*

Filename: helper.c
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <regex.h>

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "helper.h"

const gchar *get_full_data_path (const gchar *filename)
{
	gchar *fullname;

	fullname = g_build_filename (DATADIR, filename, NULL);
	g_assert (g_file_test (fullname, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR));
	return fullname;
}

const gchar *strsubstring (const gchar *sourcestring, gint start, gint end)
{
  gchar *substring = "";

  if (end <= start)
  {
    g_print ("End is smaller than start");
  }
  else
  {
    gsize length = end - start;
    substring = g_malloc0 (length);
    for (gint i = 0; i < length; i++)
    {
      substring[i] = sourcestring[start+i];
    }
    substring[length] = 0;
  }
  return substring;
}

const gchar *scan_location (const gchar *filename)
{
  struct sockaddr_un address;
  gint clamav_socket;
  const gchar *command = g_strconcat ("SCAN ", filename, "\0", NULL);
  gsize retrecv;
  gsize retbufsize = 1024;
  gchar *retbuffer;
  const gchar *clamav = "/var/run/clamav/clamd.ctl";

  /* regex */
  regex_t re_all;
  size_t nmatch = 2;
  regmatch_t pmatch[nmatch];

  address.sun_family = AF_LOCAL;
  strncpy (address.sun_path, clamav, sizeof (address.sun_path) - 1);

  clamav_socket = socket (PF_LOCAL, SOCK_STREAM, 0);
  if (clamav_socket > 0)
  {
    if (connect (clamav_socket, (struct sockaddr *) &address, sizeof (address)) == 0)
    {
      if (send (clamav_socket, command, strlen (command), 0 ) > 0)
      {
        retbuffer = g_malloc0 (retbufsize);
        retrecv = recv (clamav_socket, retbuffer, retbufsize, 0);
        if (retrecv == -1)
        {
          g_print ("Error");
          close (clamav_socket);
          return NULL;
        }
        if (regcomp (&re_all, ": ([A-Za-z0-9 -.].*)\n$" , REG_EXTENDED) != 0)
        {
          g_print ("regex 'all' compile error");
        }
        if (regexec (&re_all, retbuffer, nmatch, pmatch, 0) == 0)
        {
          const gchar *bla = strsubstring (retbuffer, pmatch[1].rm_so, pmatch[1].rm_eo);
          close (clamav_socket);
          regfree (&re_all);
          g_free (retbuffer);
          return bla;
        }
        else
        {
          g_free (retbuffer);
          return "Regex error! Scan result unknown. Submit a bug report, please.";
        }
      }
    }
    else
    {
      show_message_dialog ("Couldn't connect to socket.", g_strerror (errno));
      close (clamav_socket);
    }
  }
  else
  {
    show_message_dialog ("Couldn't create a socket", g_strerror (errno));
    close (clamav_socket);
  }
  close (clamav_socket);
}

void show_message_dialog (const gchar *message01, const gchar *message02)
{
  GtkWidget *dialog;

  GtkDialogFlags flags = GTK_DIALOG_DESTROY_WITH_PARENT;
  dialog = gtk_message_dialog_new (NULL, flags, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "%s", message01);
  gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), "%s", message02);
  gtk_dialog_run (GTK_DIALOG (dialog));
  gtk_widget_destroy (dialog);
}
