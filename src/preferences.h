/*

Filename: preferences.h
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#ifndef PREFERENCES_H
#define PREFERENCES_H

#include <gtk/gtk.h>
#include "window.h"

#define FAVS_PREFERENCES_TYPE (favs_preferences_get_type())
G_DECLARE_FINAL_TYPE (FavsPreferences, favs_preferences, FAVS, PREFERENCES, GtkDialog)

FavsPreferences *favs_preferences_new (FavsWindow *win);

#endif
