/*

Filename: window.h
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#ifndef WINDOW_H
#define WINDOW_H

#include <gtk/gtk.h>
#include "application.h"

#define FAVS_WINDOW_TYPE (favs_window_get_type())
G_DECLARE_FINAL_TYPE (FavsWindow, favs_window, FAVS, WINDOW, GtkApplicationWindow)

FavsWindow *favs_window_new (FavsApp *app);
void favs_window_open (FavsWindow *win, GFile *file);

#endif
