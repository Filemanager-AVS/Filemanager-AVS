/*

Filename: application.h
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#ifndef APPLICATION_H
#define APPLICATION_H

#include <gtk/gtk.h>

#define FAVS_APP_TYPE (favs_app_get_type ())
G_DECLARE_FINAL_TYPE (FavsApp, favs_app, FAVS, APP, GtkApplication)

FavsApp *favs_new (void);

#endif
