/*

Filename: application.c
Copyright © 2010 - 2020  Alexander Nagel
Email: filemanager-avs@acwn.de
Homepage: http://www.acwn.de/projects/filemanager-avs

This file is part of Filemanager-AVS.

*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "application.h"
#include "window.h"

struct _FavsApp
{
  GtkApplication parent;
};

G_DEFINE_TYPE (FavsApp, favs_app, GTK_TYPE_APPLICATION);


static void favs_app_init (FavsApp *app)
{
}

static void application_startup (GApplication *app)
{
  G_APPLICATION_CLASS (favs_app_parent_class)->startup (app);
}

static void application_activate (GApplication *app)
{
  FavsWindow *win;

  win = favs_window_new (FAVS_APP (app));
  gtk_window_present (GTK_WINDOW (win));
}

static void application_open (GApplication *app, GFile **files, gint n_files, const gchar *hint)
{
  GList *windows;
  FavsWindow *win;
  gint i;

  windows = gtk_application_get_windows (GTK_APPLICATION (app));
  if (windows)
    win = FAVS_WINDOW (windows->data);
  else
    win = favs_window_new (FAVS_APP (app));
  for (i = 0; i < n_files; i++)
  {
    favs_window_open (win, files[i]);
  }
  gtk_window_present (GTK_WINDOW (win));
}

static void application_finalize (GObject *object)
{
  G_OBJECT_CLASS (favs_app_parent_class)->finalize (object);
}

void favs_app_class_init (FavsAppClass *class)
{
  G_APPLICATION_CLASS (class)->startup = application_startup;
  G_APPLICATION_CLASS (class)->activate = application_activate;
  G_APPLICATION_CLASS (class)->open = application_open;
}

FavsApp *favs_new (void)
{
  g_set_application_name (PACKAGE_NAME);
  return g_object_new (FAVS_APP_TYPE, "application-id", "de.acwn.filemanager-avs",
    "flags", G_APPLICATION_HANDLES_OPEN,
    NULL);
}
